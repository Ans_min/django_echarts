from django.db import models
from django.contrib.auth.models import AbstractUser


class Car(models.Model):
    """车辆信息表"""
    brand_choices = (
        ("audi", "奥迪"),
        ("bmw", "宝马"),
        ("ford", "福特"),
        ("hyundi", "现代"),
        ("merc", "水星"),
        ("skoda", "斯柯达"),
        ("toyota", "丰田"),
        ("vauxhall", "沃克斯豪尔"),
        ("vw", "大众"),
    )
    brand = models.CharField(max_length=32, verbose_name="品牌", choices=brand_choices)
    car_model = models.CharField(max_length=32, verbose_name="模型")
    year = models.CharField(max_length=5, verbose_name="年份")
    price = models.IntegerField(verbose_name="价格")
    transmission = models.CharField(max_length=32, verbose_name="变速器")
    mileage = models.IntegerField(verbose_name="里程")
    fuel_type = models.CharField(max_length=32, verbose_name="汽油种类")
    tax = models.CharField(verbose_name="税额", max_length=32)
    mpg = models.CharField(verbose_name="英里数/每加仑油", max_length=32)
    engine_size = models.CharField(verbose_name="发送机大小", max_length=32)

    def __str__(self):
        for brand_tuple in self.brand_choices:
            if brand_tuple[0] == self.brand:
                return brand_tuple[1] + "|" + self.car_model + "|" + str(self.price)
        return self.brand + "|" + self.car_model + "|" + str(self.price)


class UserInfo(AbstractUser):
    """用户信息"""
    def __str__(self):
        return self.username
