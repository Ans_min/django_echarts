from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
from django.contrib import auth
from car_web.models import Car, UserInfo
from car_web.utils.SwitchUtil import SwitchUtil


def index(request):
    """默认跳转首页"""
    return redirect("/index/")


def car_index(request):
    """首页"""
    return render(request, "car_index.html")


def car_list(request):
    """数据查询页面"""
    su = SwitchUtil()
    # 获取筛选数据
    search_dict = {}
    get_brand = request.GET.get("brand")
    if get_brand:
        search_dict["brand"] = su.big_to_small(get_brand)
    else:
        get_brand = ""
    fuel_type = request.GET.get("fuel_type")
    if fuel_type:
        search_dict["fuel_type"] = fuel_type
    else:
        fuel_type = ""
    min_price = request.GET.get("min_price")
    if min_price:
        search_dict["price__gte"] = int(min_price)
    else:
        min_price = ""
    max_price = request.GET.get("max_price")
    if max_price:
        search_dict["price__lte"] = int(max_price)
    else:
        max_price = ""

    # 分页渲染MySQL数据
    from car_web.utils.pagination import Pagination
    queryset = Car.objects.filter(**search_dict)
    page = Pagination(request, "page", queryset, page_size=15,
                      form_switch=True, last_switch=True, first_switch=True, a_prev=True, a_next=True)

    # 获取所有的品牌
    from car_web.utils.load import Load
    load = Load("D:/Python_Project/car/car_web/static/file/archive/")
    brand_list = [su.big_to_small(i.split(".")[0]) for i in load.brand_list]
    brand_option_html_string = f"<option></option><option selected>{get_brand}</option>"

    for brand in brand_list:
        if brand == get_brand:
            continue
        else:
            brand_option_html_string += f"<option>{brand}</option>"

    # 传给前端的数据
    response = {
        "car_data": page.page_queryset,
        "page_html_string": page.html(),
        "brand_option_html_string": mark_safe(brand_option_html_string),
        "type_val": fuel_type,
        "min_price_val": min_price,
        "max_price_val": max_price,
    }
    return render(request, "car_list.html", response)


def car_view(request):
    """数据大屏页面"""
    # 加载大数据分析出的json文件
    from car_web.utils.LoadData import LoadJsonToView
    # brand柱状图数据载入
    ljv1 = LoadJsonToView("D:/Python_Project/car/car_web/static/file/output/brandGroupDataNum", "brand")

    # 最大最小价格
    min_price = Car.objects.all().order_by("price").first()
    max_price = Car.objects.all().order_by("-price").first()
    # 传给前端的数据
    su = SwitchUtil()
    response = {
        # icon数据
        "brand_num": len(su.big_list),
        "car_info_num": Car.objects.all().count,
        "min_price": min_price,
        "max_price": max_price,
        # 柱状图数据
        "brand_list": mark_safe(str([su.big_to_small(i) for i in ljv1.make_bar_data()[0]])),
        "brand_list_num": mark_safe(str([i for i in ljv1.make_bar_data()[1]])),

    }
    return render(request, "car_view.html", response)


def car_view2(request):
    """数据大屏2"""
    # 加载大数据分析出的json文件
    from car_web.utils.LoadData import LoadJsonToView
    # fuelType饼图数据载入
    ljv2 = LoadJsonToView("D:/Python_Project/car/car_web/static/file/output/fuelTypeGroupDataNum", "fuel_type")
    # icon
    fuel_type = Car.objects.values("fuel_type").distinct().count()
    year_type = Car.objects.values("year").distinct().count()
    # mileage
    mileages = Car.objects.values("mileage")
    mileage_type = {
        "<5000": 0,
        "5000-10000": 0,
        "10000-20000": 0,
        ">20000": 0,
    }
    for mileage in mileages:
        if mileage["mileage"] < 5000:
            mileage_type["<5000"] += 1
        elif mileage["mileage"] < 10000:
            mileage_type["5000-10000"] += 1
        elif mileage["mileage"] < 20000:
            mileage_type["10000-20000"] += 1
        else:
            mileage_type[">20000"] += 1
    mileage_list = []
    for k, v in mileage_type.items():
        mileage_list.append(f'{"{"}"name": "{k}", "value": {v}{"}"}')

    # year
    years = Car.objects.values("year")
    year_type_dict = {
        "<2000": 0,
        "2000-2010": 0,
        "2010-2015": 0,
        "2015-2020": 0,
        ">2020": 0,
    }
    for year in years:
        print(type(year["year"]))
        if year["year"] < "2000":
            year_type_dict["<2000"] += 1
        elif year["year"] < "2010":
            year_type_dict["2000-2010"] += 1
        elif year["year"] < "2015":
            year_type_dict["2010-2015"] += 1
        elif year["year"] < "2020":
            year_type_dict["2015-2020"] += 1
        else:
            year_type_dict[">2020"] += 1
    year_list = []
    for k, v in year_type_dict.items():
        year_list.append(f'{"{"}"name": "{k}", "value": {v}{"}"}')
    response = {
        "fuel_type": fuel_type,
        "year_type": year_type,
        # 饼图数据
        "fuel_type_str": mark_safe(ljv2.make_pie_data()),
        "mileage_list": mark_safe(",".join(mileage_list)),
        "year_list": mark_safe(",".join(year_list)),
    }
    return render(request, "car_view2.html", response)


def car_price(request):
    """价格预测页面"""
    from car_web.utils.PricePredict import PricePredict, FieldSwitch
    # 传给前端的数据
    response = {}
    fs = FieldSwitch(Car)
    su = SwitchUtil()
    # 获取前端数据
    if request.method == "POST":
        brand = fs.brand_sw(su.big_to_small(request.POST.get("brand")))
        car_model = fs.car_model_sw(request.POST.get("car_model"))
        fuel_type = fs.fuel_type_sw(request.POST.get("fuel_type"))
        transmission = fs.transmission_sw(request.POST.get("transmission"))
        year = int(request.POST.get("year"))
        mileage = int(request.POST.get("mileage"))

        # 预测
        temp = fs.numpy_load()
        pp = PricePredict(temp["x"], temp["y"])
        # 品牌、模型、年份、里程数、汽油种类、变速器
        predict = pp.predict_res([brand, car_model, year, mileage, fuel_type, transmission])[0]
        response["predict_price"] = int(predict/10000)
    else:
        response["predict_price"] = 0

    # brand select 生成
    brand_html_string = ""
    for brand in fs.brands:
        if brand == su.big_to_small(request.POST.get("brand")):
            brand_html_string += f"<option selected>{su.big_to_small(brand)}</option>\n"
        else:
            brand_html_string += f"<option>{su.big_to_small(brand)}</option>\n"
    response["brand_option_html_string"] = mark_safe(brand_html_string)

    # car_model select 生成
    car_model_html_string = ""
    for car_model in fs.car_models:
        if car_model == request.POST.get("car_model"):
            car_model_html_string += f"<option selected>{car_model}</option>\n"
        else:
            car_model_html_string += f"<option>{car_model}</option>\n"
    response["car_model_option_html_string"] = mark_safe(car_model_html_string)

    # fuel_type select 生成
    fuel_type_html_string = ""
    for fuel_type in fs.fuel_types:
        if fuel_type == request.POST.get("fuel_type"):
            fuel_type_html_string += f"<option selected>{fuel_type}</option>\n"
        else:
            fuel_type_html_string += f"<option>{fuel_type}</option>\n"
    response["fuel_type_option_html_string"] = mark_safe(fuel_type_html_string)

    # transmission select 生成
    transmission_html_string = ""
    for transmission in fs.transmissions:
        if transmission == request.POST.get("transmission"):
            transmission_html_string += f"<option selected>{transmission}</option>\n"
        else:
            transmission_html_string += f"<option>{transmission}</option>\n"
    response["transmission_option_html_string"] = mark_safe(transmission_html_string)

    # 保留传递值
    response["year"] = request.POST.get("year", "")
    response["mileage"] = request.POST.get("mileage", "")

    return render(request, "car_price.html", response)


def login(request):
    """登录界面"""
    if request.method == "POST":
        response = {
            "user": None,
            "msg": None,
        }
        user = request.POST.get("user")
        pwd = request.POST.get("pwd")
        user = auth.authenticate(username=user, password=pwd)
        if user:
            auth.login(request, user)
            response["user"] = user.username
        else:
            response["msg"] = "用户名或密码错误！"
        return JsonResponse(response)

    return render(request, 'login.html')


def register(request):
    """注册界面"""
    from car_web.utils.my_forms import UserForm
    if request.is_ajax():
        form = UserForm(request.POST)
        response = {"user": None, "msg": None}
        if form.is_valid():
            response["user"] = form.cleaned_data.get("user")
            user = form.cleaned_data.get("user")
            pwd = form.cleaned_data.get("pwd")
            UserInfo.objects.create_user(username=user, password=pwd)
        else:
            response["msg"] = form.errors
        return JsonResponse(response)
    form = UserForm()
    return render(request, "register.html", {"form": form})


def logout(request):
    """注销操作"""
    auth.logout(request)
    return redirect("/login/")


def change_pwd(request, username):
    response = {
        "username": username,
        "error": "",
    }
    if request.method == "POST":
        pwd = request.POST.get("pwd")
        re_pwd = request.POST.get("re_pwd")
        user = auth.authenticate(username=username, password=pwd)
        if user:
            u = UserInfo.objects.get(username__exact=username)
            u.set_password(re_pwd)
            u.save()
            auth.logout(request)
            return redirect("/login/")
        else:
            response["error"] = "密码错误！"
            return render(request, "change_pwd.html", response)
    return render(request, "change_pwd.html", response)
