from django.contrib import admin
from car_web.models import Car, UserInfo

admin.site.register(Car)
admin.site.register(UserInfo)
