import json


class LoadJsonToView(object):
    def __init__(self, json_dir, key):
        self.json_dir = json_dir    # D:/Python_Project/car/car_web/static/file/output
        self.key = key

    def load_json(self):
        import os
        file_list = os.listdir(self.json_dir)
        files = []
        for file in file_list:
            if file.startswith("part"):
                files.append(file)
            else:
                continue
        return files

    def contain_context(self):
        files = self.load_json()
        context = []
        for file in files:
            with open(self.json_dir+"/"+file, "r") as f:
                context += f.readlines()
        return [json.loads(text.strip()) for text in context]

    def make_pie_data(self):
        dict_list = self.contain_context()
        pie_data = ""
        for dic in dict_list:
            pie_data += json.dumps({"value": dic["count"], "name": dic[f"{self.key}"]}) + ",\n"
        return pie_data

    def make_bar_data(self):
        dict_list = self.contain_context()
        bar_x = []
        bar_y = []
        for obj in dict_list:
            bar_x.append(obj["brand"])
            bar_y.append(obj["count"])
        return bar_x, bar_y


if __name__ == '__main__':
    # ljp = LoadJsonToPie("D:/Python_Project/onion_web/media/file/output/stateGroupDataNum.json", "state")
    # print(ljp.make_pie_data())
    ljs = LoadJsonToView("D:/Python_Project/car/car_web/static/file/output/brandGroupDataNum", "brand")
    print(ljs.make_bar_data())
