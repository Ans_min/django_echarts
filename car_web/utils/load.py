from car_web.models import Car


class Load(object):
    """加载数据至MySQL"""
    def __init__(self, path):
        import os
        self.path = path
        self.brand_list = os.listdir(self.path)

    def get_dir(self):
        return [self.path+filename for filename in self.brand_list]

    @staticmethod
    def load_data(filename):
        import pandas as pd
        data = pd.read_csv(filename)
        # 获取品牌
        brand = filename.split("/")[-1].split(".")[0]
        # 根据行号来进行循环写入数据库
        for line_id in range(data.shape[0]):
            row = data.loc[line_id]
            Car.objects.create(
                brand=brand,
                car_model=row["model"].strip(),
                year=str(row["year"]).strip(),
                price=row["price"],
                transmission=row["transmission"].strip(),
                mileage=row["mileage"],
                fuel_type=row["fuelType"].strip(),
                tax=str(row["tax"]),
                mpg=str(row["mpg"]),
                engine_size=str(row["engineSize"]),
            )

    def load(self):
        for filename in self.get_dir():
            self.load_data(filename)


# if __name__ == '__main__':
#     load = Load("D:/Python_Project/car/car_web/static/file/archive/")
#     load.load()

# 调用举例：
# from car_web.utils.load import Load
# load = Load("D:/Python_Project/car/car_web/static/file/archive/")
# load.load()
