class SwitchUtil(object):
    def __init__(self):
        brand_choices = {
            "audi": "奥迪",
            "bmw": "宝马",
            "ford": "福特",
            "hyundi": "现代",
            "merc": "水星",
            "skoda": "斯柯达",
            "toyota": "丰田",
            "vauxhall": "沃克斯豪尔",
            "vw": "大众",
        }
        self.small_list = [i for i in brand_choices.keys()]
        self.big_list = [i for i in brand_choices.values()]

    def big_to_small(self, key):
        if key in self.big_list:
            index = self.big_list.index(key)
            return self.small_list[index]
        elif key in self.small_list:
            index = self.small_list.index(key)
            return self.big_list[index]
        else:
            print("传入值不属于转化列表！")
            return 0


# if __name__ == '__main__':
#     su = SwitchUtil()
#     print(su.big_to_small("宝马"))
