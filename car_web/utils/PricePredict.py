import copy
import numpy as np


class FieldSwitch(object):
    def __init__(self, model):
        self.model = model
        self.brands = [obj["brand"] for obj in model.objects.all().values("brand").distinct()]
        self.car_models = [obj["car_model"] for obj in model.objects.all().values("car_model").distinct()]
        self.fuel_types = [obj["fuel_type"] for obj in model.objects.all().values("fuel_type").distinct()]
        self.transmissions = [obj["transmission"] for obj in model.objects.all().values("transmission").distinct()]

    def brand_sw(self, key):
        if key in self.brands:
            return self.brands.index(key)
        else:
            return self.brands[key]

    def car_model_sw(self, key):
        if key in self.car_models:
            return self.car_models.index(key)
        else:
            return self.car_models[key]

    def fuel_type_sw(self, key):
        if key in self.fuel_types:
            return self.fuel_types.index(key)
        else:
            return self.fuel_types[key]

    def transmission_sw(self, key):
        if key in self.transmissions:
            return self.transmissions.index(key)
        else:
            return self.transmissions[key]

    def load_xy(self):
        # 品牌、模型、年份、里程数、汽油种类、变速器
        x = []
        # 价格
        y = []
        data = self.model.objects.all().values("brand", "car_model", "year", "price",
                                               "transmission", "mileage", "fuel_type")
        for qs in data:
            y.append(qs["price"])
            x.append([
                self.brand_sw(qs["brand"]), self.car_model_sw(qs["car_model"]),
                int(qs["year"]), self.transmission_sw(qs["transmission"]),
                qs["mileage"], self.fuel_type_sw(qs["fuel_type"]),
            ])
        return [x, y]

    def numpy_load(self):
        return {
            "x": np.array(self.load_xy()[0]),
            "y": np.array(self.load_xy()[1]),
        }

    def show(self):
        for brand in self.brands:
            print(brand + "|" + str(self.brand_sw(brand)))


class PricePredict(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def train(self):
        from sklearn import linear_model
        # 线性回归算法
        lr = linear_model.BayesianRidge()
        # 拟合
        lr.fit(self.x, self.y)
        import joblib
        joblib.dump(lr, "D:/Python_Project/car/car_web/static/file/ML/model.pkl")

    @staticmethod
    def load_model():
        import joblib
        return joblib.load("D:/Python_Project/car/car_web/static/file/ML/model.pkl")

    def predict_res(self, info_list):
        # 训练模型
        # self.train()
        lrp = self.load_model()
        np_info = np.array(info_list)
        # 深复制
        np_info_copy = copy.deepcopy(np_info)
        print(np_info_copy, ':', lrp.predict(np_info_copy.reshape(1, -1)))
        return lrp.predict(np_info_copy.reshape(1, -1))


"""
# 品牌、模型、年份、里程数、汽油种类、变速器
data_x = np.array([
    [1, 1, 2017, 15735, 1, 2], [1, 1, 2016, 29946, 1, 2],
    [1, 6, 2016, 36203, 2, 1], [1, 6, 2016, 76788, 2, 1],
    [1, 15, 2016, 23714, 2, 1], [1, 15, 2016, 23789, 2, 1],
    [1, 3, 2017, 29545, 2, 2], [1, 3, 2015, 75619, 2, 2],
    [1, 13, 2017, 21369, 1, 2], [1, 13, 2014, 39190, 2, 1],
    [1, 4, 2017, 32097, 2, 1], [1, 4, 2016, 52561, 2, 2],
    [1, 5, 2014, 83872, 2, 1], [1, 5, 2017, 13057, 1, 1],
])

# 价格
data_y = np.array([12500, 11000, 16500, 13250, 20200, 20000, 17100, 11300, 16500, 15000, 14400, 12750, 13200, 20200])

# 根据已知数据拟合最佳直线的系数和截距
lrp = PricePredict.linearRegressionPredict(data_x, data_y)
# 查看最佳拟合系数
print('k:', lrp.coef_)
# 截距
print('b:', lrp.intercept_)

xs = np.array([
    [1, 4, 2017, 15735, 1, 1],
    [1, 15, 2017, 15735, 2, 1],
    [1, 1, 2017, 15735, 2, 2]
])
for item in xs:
    # 深复制
    item1 = copy.deepcopy(item)
    if item1[0] > 18:
        item1[0] = 18
    print(item, ':', lrp.predict(item1.reshape(1, -1)))
"""
