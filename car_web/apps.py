from django.apps import AppConfig


class CarWebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'car_web'
